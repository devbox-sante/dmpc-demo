package fr.devboxsante.dmp.exemple;

import com.google.common.io.ByteStreams;
import fr.devboxsante.dmp.*;
import fr.devboxsante.dmp.model.*;
import fr.devboxsante.dmp.tool.TimeManager;
import fr.devboxsante.oid.Oids;

import java.io.IOException;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.List;

public class AlimentationScenario extends DefaultScenario implements Runnable {

    private static final String PATIENT_DATE_NAISSANCE = "19530724";
    //    private static final int PATIENT_AGE = 17;
//    private static final DMPCAdresse PATIENT_ADRESSE = new DMPCAdresse("123 rue imaginaire", "33000", "BORDEAUX", "FRANCE");
    private static final String PATIENT_INTERNAL_ID = "654321";
    private static final String PATIENT_NIR = "279035121518989";
    private static final String PATIENT_NOM_PATRONYMIQUE = "PAT-TROIS";
    private static final String PATIENT_NOM_USUEL = "PAT-TROIS";
    private static final String PATIENT_PRENOM = "DOMINIQUE";
    private static final String PATIENT_LIEU_NAISSANCE = "63220";

    private final TimeManager timeManager;

    public AlimentationScenario(DMPCClient client, TimeManager timeManager) {
        super(client);
        this.timeManager = timeManager;
    }

    @Override
    public void run() {
        try {
            // Initialisation du context PS
            DMPCContext context = initContext();

            // rendre INVISIBLE_REPRESENTANTS_LEGAUX les connexions
//            final DMPCParametres parametres = client.getParametres();
//            if (parametres.isFonctionsGestionMineurs() && PATIENT_AGE < parametres.getAgeMajorite()) {
//                final DMPCCode restriction = client.getCodesFor(DMPCCode.JeuxValeursDMP.CONFIDENTIALITY_CODE).get(5);
//                context.setConfidentialitCode(restriction);
//            }
            // Initialisation du patient
            DMPCPatient patient = DMPCPatient.builder()
                    .matriculeINS(Identifiant.builder()
                            .valeur(PATIENT_NIR)
                            .identifiantSysteme(Oids.ANS_1_2_250_1_213._1_4_10_INS_NIR_TEST.val())
                            .build())
                    .internalId(PATIENT_INTERNAL_ID)
                    .nomPatronymique(PATIENT_NOM_PATRONYMIQUE)
                    .nomUsuel(PATIENT_NOM_USUEL)
                    .prenom(PATIENT_PRENOM)
                    .sexe(DMPCSexe.F)
                    .dateDeNaissance(PATIENT_DATE_NAISSANCE)
                    .lieuNaissance(PATIENT_LIEU_NAISSANCE)
                    //                    .adresse(PATIENT_ADRESSE)
                    .build();

            // Test d'existence et droit d'accés pour le PS
            final TD02Response td02Response = client.td02Exist(context, TD02Request.builder()
                    .matriculeINS(patient.getMatriculeINS())
                    .build());

            /////////////////////////////////////////////////////////////////////
            // Alimentation du DMP du patient
            final ZonedDateTime maintenant = timeManager.getLocal();

            //   création du document
            InputStream input = this.getClass().getClassLoader().getResourceAsStream("1.pdf");
            byte[] content = new byte[input.available()];
            ByteStreams.readFully(input, content);

            DMPCDocument document = DMPCDocument.builder()
                    .patient(patient)
                    .classCodeCode(client.getCodesFor(DMPCCode.JeuxValeursDMP.CLASS_CODE).get(0))
                    .confidentialityCodes(List.of(findCode(DMPCCode.JeuxValeursDMP.CONFIDENTIALITY_CODE, "N")))
                    .commentaire("Commentaire du Document")
                    .events(List.of(new DMPCCode("B18", "2.16.840.1.113883.6.3", "Hépatite virale chronique")))
                    .practiceSettingCode(findCode(DMPCCode.JeuxValeursDMP.PRACTICE_SETTING_CODE, "AMBULATOIRE"))
                    .serviceStartDateTime(maintenant.minusHours(3))
                    .serviceStopDateTime(maintenant.minusHours(2))
                    .titre("Titre du Document")
                    .typeCode(findCode(DMPCCode.JeuxValeursDMP.TYPE_CODE, "11488-4"))
                    .creationDateTime(maintenant)
                    .formatCode(findCode(DMPCCode.JeuxValeursDMP.FORMAT_CODE, "urn:ihe:iti:xds-sd:pdf:2008"))
                    .content(content).build();

            //    création de la soumission
            DMPCSoumission soumission = DMPCSoumission.builder()
                    .documents(List.of(document))
                    .titre("Soumission du document")
                    .commentaire("Commentaire de la soumission")
                    .patient(patient)
                    .contentType("04").build();

            //   envoi de la soumission
            TD21Response td21Response = client.td21SubmitDocuments(context, new TD21Request(soumission));
            System.out.println("Soumission enregistré avec l'uuid :" + td21Response.getSoumission().getUuid());

            td21Response.getSoumission().getDocuments().forEach(submitted -> {
                DMPCDocument submittedDocument = (DMPCDocument) submitted;
                System.out.println(String.format("Contenant le document ayant pour uniqueId : %s et uuid : %s",
                        submittedDocument.getUniqueId(), submittedDocument.getEntryUuid()));
            });

        } catch (DMPCClientException | IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public DMPCCode findCode(DMPCCode.JeuxValeursDMP jv, String code) throws DMPCClientException {
        // Vérifie que le code appartient à la nomenclature avant de l'envoyer au DMP.
        return client.getCodesFor(jv).stream().filter(dmpcCode -> dmpcCode.getValeur().equals(code)).findFirst().get();
    }

}
