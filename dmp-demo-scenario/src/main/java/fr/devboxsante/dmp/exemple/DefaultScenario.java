package fr.devboxsante.dmp.exemple;

import fr.devboxsante.dmp.DMPCClient;
import fr.devboxsante.dmp.DMPCClientException;
import fr.devboxsante.dmp.DMPCContext;
import fr.devboxsante.dmp.model.DMPCCode;
import fr.devboxsante.dmp.model.DMPCPersonnelSante;

public abstract class DefaultScenario {

    protected static final String PS_INTERNAL_ID = "123456";
    protected static final String PS_LASTNAME = "MED-CS RPPS0029614";
    protected static final String PS_FIRSTNAME = "ANNE";
    protected final static String PS_ROLE = "10";  // médecint
    protected final static String PS_SPECIALITE = "G15_10/SM26";   // médecine générale

    protected static final String PS_STRUCTURE_NOM = "CENTRE DE SANTE RPPS15683";
    protected static final String PS_STRUCTURE_ID = "10B0156832";

    protected final static String PS_STRUCTURE_SECTEUR_ACTIVITE = "SA05";
    protected DMPCClient client;

    public DefaultScenario(DMPCClient client) {
        this.client = client;
    }

    public DMPCContext initContext() throws DMPCClientException {
        DMPCContext context = new DMPCContext();
        DMPCPersonnelSante ps = new DMPCPersonnelSante(PS_INTERNAL_ID, PS_LASTNAME, PS_FIRSTNAME);
        ps.setRole(PS_ROLE);
        ps.setSpecialite(PS_SPECIALITE); // G15_10/SM30 Néphrologie
        ps.setSecteurActivite(PS_STRUCTURE_SECTEUR_ACTIVITE);
//        ps.getStructureSante().setIdNational(PS_STRUCTURE_ID);
//        ps.getStructureSante().setNom(PS_STRUCTURE_NOM);
        context.setAuthor(ps);
        return context;
    }
    
    
}
