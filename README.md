# Projet d'exemple de la DMPC-API

Deux modes d'intégration : 

* embedded : permet d'utiliser l'api comme un composant springframework dans une application spring boot
* proxy-rest :permet d'accéder à l'api par l'intermédiaire d'un client des APIs REST

Un module `dmpc-demo-module` donne des scenarii fonctionnels qui peuvent être joué via les deux modes d'intégration.


## Prérequis 

* Un JDK 11
* Maven 3.5 ou supérieur

## Récupération des dépendances

Pour récupérer les binaires, il vous faut accéder aux différerents repositories http://repo.dev.coop/nexus définis dans le `pom.xml` :

```
<repositories>
       <repository>
            <id>devcoop-VotreRepository-releases</id>
            <url>http://repo.dev.coop/nexus/content/repositories/VotreRepository-releases/</url>
        </repository>
        <repository>
            <id>devcoop-public</id>
            <url>http://repo.dev.coop/nexus/content/repositories/public/</url>
        </repository>
        <repository>
            <id>devcoop-shared-releases</id>
            <url>http://repo.dev.coop/nexus/content/repositories/shared-releases/</url>
        </repository>
</repositories>

```

Pour celà vous devez obtenir un compte d'accés auprès de DEVCOOP (devbox-sante@devcoop.fr). Une fois obtenu vous pouvez configurer votre `settings.xml` de maven de la manière suivante :

```
<settings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">

    <servers> 
        <server>
            <id>devcoop-votreRepository-releases</id>
            <username>VotreUser</username>
            <password>VotrePassword</password>
        </server>
        <server>
            <id>devcoop-public</id>
            <username>VotreUser</username>
            <password>VotrePassword</password>
        </server>
        <server>
            <id>devcoop-shared-releases</id>
            <username>VotreUser</username>
            <password>VotrePassword</password>
...
</server>
```


## Mode embedded 

Ce projet illustre l'utilisation de la DMPC-API en mode embedded. Il s'agit d'une application spring-boot, 

Dont le code source ressemble à :

```
@SpringBootApplication
@ComponentScan(basePackages = {"fr.devcoop.dmpc.demo", "fr.devcoop.dmpc"})
public class DmpcEmbeddedDemoApplication {

    @Autowired
    private DMPCClient client;

    public static void main(String[] args) {
        SpringApplication.run(DmpcEmbeddedDemoApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            // ...
            client.td02Exist(context, new TD02Request(patient.getIns()))
            client.td21SubmitDocuments(context, new TD21Request(submission));
        };
    }
```

La documentation de l'ensemble des [transactions disponibles](https://doc.devbox-sante.fr/dmp) est disponible.

### Configuration de l'api :

La configuration se trouve dans le fichier `application.yml` . Ce fichier contient tous les paramètres variables de l'api notamment les certificats et clés de signatures/authentifications.


### Démarrer

```
mvn clean install
cd dmp-demo-embedded
mvn spring-boot:run
```

