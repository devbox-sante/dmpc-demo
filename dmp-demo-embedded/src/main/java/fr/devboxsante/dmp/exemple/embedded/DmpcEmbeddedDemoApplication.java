package fr.devboxsante.dmp.exemple.embedded;

import fr.devboxsante.dmp.DMPCClient;
import fr.devboxsante.dmp.tool.TimeManager;
import fr.devboxsante.dmp.exemple.AlimentationScenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"fr.devboxsante"})
public class DmpcEmbeddedDemoApplication {

    @Autowired
    private DMPCClient client;
    @Autowired
    private TimeManager timeManager;

    public static void main(String[] args) {
        SpringApplication.run(DmpcEmbeddedDemoApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return (args) -> new AlimentationScenario(client, timeManager).run();

    }

}
